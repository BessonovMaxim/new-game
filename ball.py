from random import randint

import pygame


class Ball:
    filename = 'basketball.png'

    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.speed = [randint(0,1)*2-1, randint(0,1)*2-1]
        self.image = pygame.image.load(Ball.filename)
        self.br = self.image.get_rect()

    def process_event(self, event):
        pass

    def logic(self, WIDTH, HEIGHT):
        self.x += self.speed[0]
        self.y += self.speed[1]
        if self.x > WIDTH - 50 or self.x < 0:
            self.speed[0] *= -1
        if self.y > HEIGHT - 50 or self.y < 0:
            self.speed[1] *= -1
        self.update_rect()

    def update_rect(self):
        self.br.x = self.x - self.br.w // 2
        self.br.y = self.y - self.br.h // 2

    def draw(self, screen):
        screen.blit(self.image, self.br)