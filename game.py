from random import randint

import pygame
import sys
from button import Button
from ball import Ball
from constants import BLACK, RED, GREEN
from constants import GAME_STATE


class Game:
    def __init__(self, w=400, h=300):  # Создание окна
        self.WIDTH = w
        self.HEIGHT = h
        self.font = pygame.font.SysFont("Comic Sans", 40)
        self.SIZE = self.WIDTH, self.HEIGHT
        self.screen = pygame.display.set_mode(self.SIZE, pygame.RESIZABLE)  # Установка размеров окна
        self.gameover = False
        self.game_state = GAME_STATE["MENU"]
        self.start_button = Button((0,0,200,50), RED, self.start_game, text = "START", hover_color = GREEN)
        self.settings_button = Button((0,50,200,50), RED, self.settings, text = "SETTINGS", hover_color = GREEN)
        self.about_button = Button((0,100,200,50), RED, self.about, text = "ABOUT", hover_color = GREEN)
        self.exit_button = Button((0,150,200,50), RED, self.exit, text = "EXIT", hover_color = GREEN)
        self.back_button = Button((200,250,200,50), RED, self.back, text = "BACK", hover_color = GREEN)
        self.prepare()

    def start_game(self):
        self.game_state = GAME_STATE["GAME"]

    def settings(self):
        self.game_state = GAME_STATE["SETTINGS"]

    def about(self):
        self.game_state = GAME_STATE["ABOUT"]

    def back(self):
        self.game_state = GAME_STATE["MENU"]

    def exit(self):
        sys.exit()  # Завершение программы

    def prepare(self):  # Создание остальных игровых объектов
        self.objects = []
        for i in range(5):
            self.objects.append(Ball(randint(0, self.WIDTH-50), randint(0, self.HEIGHT-50)))

    def main_loop(self):
        while not self.gameover:
            self.process_events()
            self.process_logic()
            self.process_drawing()
            pygame.time.wait(10)  # ожидание 10 мсек

    def process_events(self):
        for event in pygame.event.get():  # цикл обработки событий
            if event.type == pygame.QUIT:  # проверка на событие выхода
                self.gameover = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    self.game_state = GAME_STATE["MENU"]
            for item in self.objects:
                item.process_event(event)
            if self.game_state == GAME_STATE["MENU"]:
                self.start_button.check_event(event)
                self.settings_button.check_event(event)
                self.about_button.check_event(event)
                self.exit_button.check_event(event)
            else:
                self.back_button.check_event(event)



    def process_logic(self):
        for item in self.objects:
            item.logic(self.WIDTH, self.HEIGHT)

    def process_drawing(self):
        self.screen.fill(BLACK)  # Заливка черным цветом

        if (self.game_state == GAME_STATE["MENU"]):
            self.start_button.update(self.screen)
            self.settings_button.update(self.screen)
            self.about_button.update(self.screen)
            self.exit_button.update(self.screen)
        elif (self.game_state == GAME_STATE["SETTINGS"]):
            settings = self.font.render("this text is cool:)",False, GREEN)
            self.screen.blit(settings, (100, 100))
            self.back_button.update(self.screen)
        elif (self.game_state == GAME_STATE["ABOUT"]):
            about = self.font.render("this text is cooler:)",False, GREEN)
            self.screen.blit(about, (100, 100))
            self.back_button.update(self.screen)
        else:
            for item in self.objects:
                item.draw(self.screen)
            self.back_button.update(self.screen)
        pygame.display.flip()  # отображение на экране (двойная буферизация)