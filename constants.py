BLACK = 0, 0, 0
RED = 255, 0, 0
GREEN = (0, 255, 0)

GAME_STATE = {
    "MENU": 0,
    "GAME": 1,
    "SETTINGS": 2,
    "ABOUT": 3,
    "EXIT": 4
}