import pygame
import sys

from game import Game


def hello():
    pygame.init()  # Инициализация библиотеки
    pygame.font.init()
    g = Game(400, 300)
    g.main_loop()
    sys.exit()  # Завершение программы


if __name__ == "__main__":
    hello()
